angular.module('sitecView').factory('operationsService', ['c8yDeviceControl', function (c8yDeviceControl) {
    'use strict';

    return {
        getOperations: function(deviceID, limit,status) {
            var limit = (limit === undefined) ? 1000 : limit;
            var operationFilter = {
                deviceId: deviceID,
                revert: 'true',
                nocache: new Date().getTime(),
                dateFrom: '1970-01-01',
                dateTo: '2050-01-01',
                status: status,
                pageSize: limit
            };

            var operationsCall = c8yDeviceControl.list(operationFilter);
            return operationsCall;
        },
        sendText: function(deviceID,content){
            var operation = {
                deviceId: deviceID,
                description: 'Display Text',
                EpaperDisplay: {
                    "size": "20", //Font-Size
                    "text": content
                }
            };
            return c8yDeviceControl.create(operation);
        },
        sendImage: function(deviceID,imageURL){
            var operation = {
                deviceId: deviceID,
                description: 'Display Image',
                EpaperDisplay: {
                    "imagePath": imageURL
                }
            };
            return c8yDeviceControl.create(operation);
        },
        sendZIP: function(deviceID,zipURL){
            var operation = {
                deviceId: deviceID,
                description: 'ZIP Slideshow',
                Slideshow: {
                    "zipPath": zipURL
                }
            };
            return c8yDeviceControl.create(operation);
        },
        sendURL: function(deviceID,url){
            var operation = {
                deviceId: deviceID,
                description: 'open URL',
                urlOperation: {
                    "hostUrl": url
                }
            };
            return c8yDeviceControl.create(operation);
        },
        sendSlideshow: function(deviceID,zipURL){
            var operation = {
                deviceId: deviceID,
                description: 'HTML Slideshow',
                htmlSlideshow: {
                    "zipPath": zipURL
                }
            };
            return c8yDeviceControl.create(operation);
        },
        sendSoftware: function(deviceID,swURL,jarURL,basename){
            var operation = {
                deviceId: deviceID,
                description: 'SoftwareUpdate',
                updatesoftware: {
                    "urljad": swURL,
                    "urljar": jarURL,
                    "basenamejad":basename
                }
            };
            return c8yDeviceControl.create(operation);
        },
        sendConfig: function(deviceID,urlcustomconfig,urlmainconfig){
            var operation = {
                deviceId: deviceID,
                description: 'ConfigUpdate',
                updateconfig: {
                    "urlmainconfig": urlmainconfig,
                    "urlcustomconfig": urlcustomconfig
                }
            };
            return c8yDeviceControl.create(operation);
        },
        sendCommand: function(deviceID,command){
            var operation = {
                deviceId: deviceID,
                description: 'Command',
                tsystems_s6_command: {
                    "cmd": command,
                    "arg1": "-not used-"
                }
            };
            return c8yDeviceControl.create(operation);
        }
    }
}]);
