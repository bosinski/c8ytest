angular.module('sitecView').factory('inventoryService', ['c8yInventory', function (c8yInventory) {
    'use strict';
    // define fragmentType name for filter
    var binaryFragmentType= '_attachments';

    return {
        getBinaries: function(limit) {
            var limit = (limit === undefined) ? 1000 : limit;
            var binariesFilter = {
                fragmentType: binaryFragmentType,
                pageSize: limit
            };

            var binariesCall = c8yInventory.list(binariesFilter);
            return binariesCall;
        }
    }
}]);
