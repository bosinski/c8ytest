angular.module('sitecView', [])
    .config(['c8yViewsProvider', function (c8yViewsProvider) {
        c8yViewsProvider.when('/device/:deviceId', {
            name: 'Sitec-Control',
            icon: 'gamepad',
            priority: 2,
            templateUrl: ':::PLUGIN_PATH:::/views/sitecView.html',
            controller: 'sitecViewCtrl',
            // show by type
            /*
            showIf: ['$routeParams', 'c8yBase', 'c8yDevices', function ($routeParams, c8yBase, c8yDevices) {
                return c8yDevices.detailCached($routeParams.deviceId)
                    .then(c8yBase.getResData)
                    .then(function (device) {
                        return device.type === 'tsystems_sitec_s6';
                    });
            }]
            */
            // or show by supportedOperations
            showIf: ['$routeParams', 'c8yBase', 'c8yDevices', function ($routeParams, c8yBase, c8yDevices) {
                return c8yDevices.detailCached($routeParams.deviceId)
                    .then(c8yBase.getResData)
                    .then(function (device) {
                        c8yDevices.supportsOperation(device, 'tsystems_sitec_support');
                    });
            }]
        });
    }]);