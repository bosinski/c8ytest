angular.module('sitecView')
    .controller('sitecViewCtrl', ['$scope', '$routeParams', 'c8yDevices', 'c8yAlert','inventoryService','operationsService','c8yBinary','$location','info','$http',
        function ($scope, $routeParams, c8yDevices, c8yAlert,inventoryService,operationsService,c8yBinary,$location,info,$http) {
            'use strict';
            $scope.binaries = [];
            $scope.filetypes = [
                {'extension': 'jar', 'contentType': 'application/java-archive'},
                {'extension': 'jad', 'contentType': 'text/plain;charset=UTF-8'},
                {'extension': 'cfg', 'contentType': 'text/plain;charset=UTF-8'}
            ];
            $scope.errorMsg='';
            $scope.msg='';
            $scope.sortorder = '-lastUpdated';
            $scope.isNewFile = true;

            //Common
            $scope.byType = function(type) {
                return function(binary) {
                    var extension = binary.name.substring(binary.name.lastIndexOf('.')+1,binary.name.length);
                    if(extension==type) {
                        return true;
                    }
                    return false;
                }
            }
            $scope.orderBy = function (property) {
                $scope.sortorder = ($scope.sortorder[0] === '+' ? '-' : '+') + property;
            };
            function getBinaries() {
                inventoryService.getBinaries(100).then(function (binaryList) {
                    if (binaryList.length > 0) {
                        $scope.binaries = binaryList;
                    }
                });
            }

            //SITEC-Command-Section
            $scope.sendOperation = function() {
                $scope.errorMsg='';
                //console.log("got Operation: "+JSON.stringify($scope.operationCommand));
                if($scope.operationCommand != undefined && $scope.operationCommand.type=='jad') {
                    if($scope.operationCommand.selectedJAD =='' || $scope.operationCommand.selectedJAD == undefined || $scope.operationCommand.selectedJAD.id == undefined) {
                        $scope.errorMsg ='Software-Operation: Bitte eine JAD auswählen!';
                        return;
                    }
                    if($scope.operationCommand.selectedJAR =='' || $scope.operationCommand.selectedJAR == undefined || $scope.operationCommand.selectedJAR.id == undefined) {
                        $scope.errorMsg ='Software-Operation: Bitte eine JAR auswählen!';
                        return;
                    }

                    var selectedJAR = $scope.operationCommand.selectedJAR.name.substr(0,$scope.operationCommand.selectedJAR.name.lastIndexOf("."));
                    var selectedJAD = $scope.operationCommand.selectedJAD.name.substr(0,$scope.operationCommand.selectedJAD.name.lastIndexOf("."));
                    console.log("selectedJAR: "+selectedJAR+" / selectedJAD"+selectedJAD);
                    if(selectedJAD != selectedJAR) {
                        $scope.errorMsg ='Software-Operation: JAR/JAD Dateinamen müssen übereinstimmen!';
                        return;
                    }
                    var jadurl = baseProtocol+'://'+$location.host()+'/inventory/binaries/'+$scope.operationCommand.selectedJAD.id;
                    var jarurl = baseProtocol+'://'+$location.host()+'/inventory/binaries/'+$scope.operationCommand.selectedJAR.id;
                    console.log("send url-Command device: "+$routeParams.deviceId+" jadurl: "+jadurl+ " / jarurl: "+jarurl+" / Basename: "+selectedJAD);
                    operationsService.sendSoftware($routeParams.deviceId,jadurl,jarurl,selectedJAD).then(function() {
                        getOperations();
                    });
                }else if($scope.operationCommand != undefined && $scope.operationCommand.type=='cfg') {
                    if($scope.operationCommand.selectedMainConfig =='' || $scope.operationCommand.selectedMainConfig == undefined || $scope.operationCommand.selectedMainConfig.id == undefined) {
                        $scope.errorMsg ='Config-Operation: Bitte eine MainConfig auswählen!';
                        return;
                    }
                    if($scope.operationCommand.selectedCustomConfig =='' || $scope.operationCommand.selectedCustomConfig == undefined || $scope.operationCommand.selectedCustomConfig.id == undefined) {
                        $scope.errorMsg ='Config-Operation: Bitte eine CustomConfig auswählen!';
                        return;
                    }
                    var urlmainconfig = baseProtocol+'://'+$location.host()+'/inventory/binaries/'+$scope.operationCommand.selectedMainConfig.id;
                    var urlcustomconfig = baseProtocol+'://'+$location.host()+'/inventory/binaries/'+$scope.operationCommand.selectedCustomConfig.id;
                    console.log("send url-Commands device: "+$routeParams.deviceId+" urlmainconfig: "+urlmainconfig+" / urlcustomconfig: "+urlcustomconfig);
                    operationsService.sendConfig($routeParams.deviceId,urlcustomconfig,urlmainconfig).then(function() {
                        getOperations();
                    });
                } else if($scope.operationCommand != undefined && $scope.operationCommand.type=='command') {
                    if ($scope.operationCommand.command == '' || $scope.operationCommand.command == undefined) {
                        $scope.errorMsg = 'Bitte eine Kommando angeben!';
                        return;
                    }
                    console.log("send Text-Command device: " + $routeParams.deviceId + " command: " + $scope.operationCommand.command);
                    var command = 'command='+$scope.operationCommand.command;
                    operationsService.sendCommand($routeParams.deviceId, command).then(function() {
                        getOperations();
                    });
                }
                $scope.operationCommand = undefined;
            }
            function getOperations() {
                operationsService.getOperations($routeParams.deviceId,5,'PENDING').then(function (operationList) {
                    $scope.operations = operationList;
                });
            }

            //Binary-Section
            $scope.onFileSelect = function(files) {
                $scope.errorMsg='';
                $scope.uploadFile = files[0]
                var extension = $scope.uploadFile.name.substring($scope.uploadFile.name.lastIndexOf('.')+1,$scope.uploadFile.name.length);
                var type = getContentType(extension);
                console.log("found content-type: "+type+" Extension: "+extension);
                if(type==undefined||type==null) {
                    $scope.errorMsg='Non-Valid File Extension: '+extension;
                }

            }

            $scope.addFile = function() {
                //console.log("$scope.uploadFile: "+JSON.stringify($scope.uploadFile));
                var extension = $scope.uploadFile.name.substring($scope.uploadFile.name.lastIndexOf('.')+1,$scope.uploadFile.name.length);
                var type = getContentType(extension);
                var object = {"comment":"widget-upload","name":$scope.uploadFile.name,"type":type};
                //need to do this cause of missing type in upload-File
                var correctedfile = new File([$scope.uploadFile],$scope.uploadFile.name, { type: type});
                c8yBinary.upload(correctedfile).then(function (res) {
                    console.log("upload-Response: "+JSON.stringify(res));
                    getBinaries();
                });

            }
            $scope.toggleElement = function (element) {
                if($scope.openItem == element) {
                    $scope.openItem = "";
                } else {
                    $scope.openItem = element;
                }

            }
            function getContentType(extension) {
                for (var i = 0, len = $scope.filetypes.length; i < len; i++) {
                    if ($scope.filetypes[i].extension === extension) {
                        return $scope.filetypes[i].contentType;
                    }
                }
                return null; //nothing found
            }

            //Config-Edit-Section
            $scope.editFile = function(id) {
                $scope.isNewFile = false;
                console.log("edit File with id"+id);
                c8yBinary.detail(id).then(function (res) {
                    if(res.data != undefined && res.data.name != undefined) {
                        c8yBinary.downloadAsDataUri(res.data).then(function (uri) {
                            console.log("the URI: "+JSON.stringify(uri));
                        })
                        //get the content
                        $scope.file = res.data;
                        var binaryurl = $location.protocol()+'://'+$location.host()+'/inventory/binaries/'+id;
                        var headerString = {'Authorization': 'Basic '+info.token};
                        console.log("use Download-URI: "+binaryurl);
                        $http({method:'GET', url:binaryurl, headers:headerString})
                            .success(function(data, status, headers, config) {
                                //console.log("Downloaded: "+data);
                                $scope.filecontent = data;
                            })
                            .error(function(data, status, headers, config) {
                                console.log("an Error!!");
                            });
                    }
                });
            }
            $scope.saveFile = function() {
                //console.log("Filecontent: "+$scope.filecontent);
                $scope.errorMsg='';
                $scope.msg = '';
                if($scope.file == undefined || $scope.file.name == undefined || $scope.file.name=='' || $scope.filecontent == undefined || $scope.filecontent==''){
                    $scope.errorMsg='Non-Valid File Content or Filename';
                    return;
                }
                if($scope.filecontent != undefined && $scope.filecontent.length > 5000) {
                    $scope.errorMsg=$scope.filecontent.length + ' Char in Content. Max 5000 allowed!';
                    return;
                }
                var extension = $scope.file.name.substring($scope.file.name.lastIndexOf('.')+1,$scope.file.name.length);
                if(extension == undefined || (extension != undefined && extension!='cfg')) {
                    $scope.errorMsg='Non-Valid File Extension: '+extension+'. Please use cfg';
                    return;
                }
                //console.log("$scope.file:"+$scope.file);
                var correctedfile = new File([$scope.filecontent],$scope.file.name, { type: 'text/plain;charset=UTF-8'});
                c8yBinary.upload(correctedfile).then(function (res) {
                    console.log("upload-Response: "+JSON.stringify(res));
                    $scope.msg = 'File saved!';
                    getBinaries();
                    $scope.file='';
                    $scope.filecontent = '';
                    $scope.isNewFile = true;
                });
            }

            $scope.saveAndDelete = function() {
                if($scope.file != undefined && $scope.file.id != undefined) {
                    c8yBinary.remove($scope.file.id).then(function (res) {
                        $scope.saveFile();
                    });
                }
            }

            $scope.deleteFile = function(id) {
                c8yBinary.remove(id).then(function (res) {
                    console.log("remove-Response: "+JSON.stringify(res));
                    getBinaries();
                });
            }

            $scope.clearForm = function() {
                $scope.isNewFile = true;
                $scope.filecontent = '';
                $scope.file = '';
                $scope.errorMsg='';
                $scope.msg = '';
            }


            getBinaries();
            getOperations();

        }
    ]);